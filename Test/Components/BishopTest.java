package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class BishopTest {

    Bishop whiteBishop = new Bishop(Color.WHITE, new Position(1,1));
    Bishop blackBishop = new Bishop(Color.BLACK, new Position(2,2));

    @Test
    void move() {
        //Board Setup
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 9 && i != 18)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }

        squares[9] = new Square(Color.GREEN, whiteBishop, new Position(1,1));
        squares[18] = new Square(Color.GREEN, blackBishop, new Position(2,2));
        Board board = new Board(squares);

        //Check out of bounds
        Board testBoard = whiteBishop.move(board, new Position(9,9));
        assertEquals(testBoard, null);

        //Check invalid move
        board = new Board(squares);
        testBoard = whiteBishop.move(board, new Position(2,1));
        assertEquals(testBoard, null);

        //Check up and left movement
        testBoard = whiteBishop.move(board, new Position(2,0));
        assertNotEquals(testBoard, null);

        //Check down and right movement
        testBoard = whiteBishop.move(testBoard, new Position(1,1));
        assertNotEquals(testBoard, null);

        //Check up and right movement
        testBoard = whiteBishop.move(testBoard, new Position(2,2));
        assertNotEquals(testBoard, null);

        //Check down and left movement
        testBoard = whiteBishop.move(testBoard, new Position(1,1));
        assertNotEquals(testBoard, null);

        //Check invalid movement (Jumping over piece)
        squares[18] = new Square(Color.GREEN, blackBishop, new Position(2,2));
        board = new Board(squares);
        testBoard = whiteBishop.move(board, new Position(3,3));
        assertEquals(testBoard, null);

    }
}