package Components;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    public Square[] squares = new Square[64];
    public Board board = new Board(squares);
    public Piece piece = new Piece(Color.WHITE, new Position(1,1));

    @org.junit.jupiter.api.Test
    void setSquares() {
        board.setSquares(squares);
    }

    @org.junit.jupiter.api.Test
    void getSquares() {
        board.getSquares();
    }

    @org.junit.jupiter.api.Test
    void getPiecesOfColor() {
        Square[] pieces = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 9)
            {
                pieces[i] = new Square(Color.GREEN, null, position);
            }
        }
        pieces[9] = new Square(Color.WHITE, piece, new Position(1,1));
        Board newBoard = new Board(pieces);
        ArrayList<Piece> retArray = newBoard.getPiecesOfColor(Color.WHITE);
        assertNotNull(retArray);
    }
}