package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class KingTest {

    King blackKing = new King(Color.BLACK, new Position(7,7));
    Queen whiteQueen = new Queen(Color.WHITE, new Position(7, 5));
    Knight whiteKnight = new Knight(Color.WHITE, new Position(5, 6));
    Rook whiteRook = new Rook(Color.WHITE, new Position(5,7));
    Bishop whiteBishop = new Bishop(Color.WHITE, new Position(5,5));

    @Test
    void isKingInCheck() {
        // Setup Board
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 45 && i != 46 && i != 47 && i != 61 && i != 63)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }

        //Check bishop puts piece in check
        squares[45] = new Square(Color.GREEN, whiteBishop, new Position(5,5));
        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        Board board = new Board(squares);
        boolean kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, true);

        //Check knight puts in check
        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
        squares[46] = new Square(Color.GREEN, whiteKnight, new Position(5,6));
        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        board = new Board(squares);
        kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, true);

        //Check Rook puts in check
        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
        squares[47] = new Square(Color.GREEN, whiteRook, new Position(5,7));
        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        board = new Board(squares);
        kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, true);

        //Check queen puts in check
        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
        squares[61] = new Square(Color.GREEN, whiteQueen, new Position(7,5));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        board = new Board(squares);
        kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, true);

        //Check pawn puts in check
        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
        squares[54] = new Square(Color.GREEN, new Pawn(Color.WHITE, new Position(6,6)), new Position(6,6));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        board = new Board(squares);
        kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, true);

        //Check that it recognizes when not in check
        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
        squares[54] = new Square(Color.GREEN, null, new Position(6,6));
        squares[63] = new Square(Color.GREEN, whiteRook, new Position(7,7));
        squares[27] = new Square(Color.GREEN, blackKing, new Position(3,3));

        board = new Board(squares);
        kingCheck = blackKing.isKingInCheck(board);
        assertEquals(kingCheck, false);


    }

    @Test
    void move() {
        //Setup Board
        blackKing = new King(Color.BLACK, new Position(3,3));
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 27)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }
        squares[27] = new Square(Color.GREEN, blackKing, new Position(3,3));
        Board board = new Board(squares);

        //Check out of bounds
        Board testBoard = blackKing.move(board, new Position(9,9));
        assertEquals(testBoard, null);

        //Check no move
        testBoard = blackKing.move(board, new Position(3,3));
        assertEquals(testBoard, null);

        //Check invalid move
        board = new Board(squares);
        testBoard = blackKing.move(board, new Position(7,1));
        assertEquals(testBoard, null);

        //Check forward movement with no capture
        board = new Board(squares);
        testBoard = blackKing.move(board, new Position(4,3));
        assertNotEquals(testBoard, null);

        //Check backwards movement
        testBoard = blackKing.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check move right
        testBoard = blackKing.move(testBoard, new Position(3,4));
        assertNotEquals(testBoard, null);

        //Check move left
        testBoard = blackKing.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check up and left movement
        testBoard = blackKing.move(board, new Position(4,2));
        assertNotEquals(testBoard, null);

        //Check down and right movement
        testBoard = blackKing.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check up and right movement
        testBoard = blackKing.move(testBoard, new Position(4,4));
        assertNotEquals(testBoard, null);

        //Check down and left movement
        testBoard = blackKing.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check to make sure no hop is possible
        Pawn blackPawn = new Pawn(Color.BLACK, new Position(4,4));
        squares[36] = new Square(Color.GREEN, blackPawn, new Position(4,4));
        board = new Board(squares);
        testBoard = blackKing.move(board, new Position(3,3));
        assertEquals(testBoard, null);

    }
}