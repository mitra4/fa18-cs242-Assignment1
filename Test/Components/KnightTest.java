package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class KnightTest {

    Knight whiteKnight = new Knight(Color.WHITE, new Position(1,1));
    Knight blackNight = new Knight (Color.BLACK, new Position(3,2));

    @Test
    void move() {
        //Check move off board
        Square[] squares = new Square[63];
        Board board = new Board(squares);
        squares[9] = new Square(Color.GREEN, whiteKnight, new Position(1,1));
        board = new Board(squares);
        Board testBoard = whiteKnight.move(board, new Position(9,9));
        assertEquals(testBoard, null);

        //Check invalid move
        testBoard = whiteKnight.move(board, new Position(2,2));
        assertEquals(testBoard, null);

    }
}