package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {

    Main main = new Main();

    @Test
    void boardSetup() {
        Board board = main.boardSetup();
        Square[] squares = board.getSquares();
        assertEquals(squares.length, 64);

    }

    @Test
    void checkMove() {
        King blackKing = new King(Color.BLACK, new Position(7,7));
        Rook whiteRook = new Rook(Color.WHITE, new Position(1,1));
        // Setup Board
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 9 && i != 63)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }
        squares[9] = new Square(Color.GREEN, whiteRook, new Position(1,1));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));
        Board board = new Board(squares);
        boolean checkMove = main.checkMove(board, Color.BLACK);
        assertEquals(checkMove, false);

    }

    @Test
    void checkMate() {
        King blackKing = new King(Color.BLACK, new Position(7,7));
        Queen whiteQueen = new Queen(Color.WHITE, new Position(7, 5));
        Knight whiteKnight = new Knight(Color.WHITE, new Position(5, 6));
        Rook whiteRook = new Rook(Color.WHITE, new Position(5,7));
        Bishop whiteBishop = new Bishop(Color.WHITE, new Position(5,5));
        // Setup Board
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 45 && i != 46 && i != 47 && i != 61 && i != 63)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }

        //Check for Checkmate which should be true
        squares[45] = new Square(Color.GREEN, whiteBishop, new Position(5,5));
        squares[46] = new Square(Color.GREEN, whiteKnight, new Position(5,6));
        squares[47] = new Square(Color.GREEN, whiteRook, new Position(5,7));
        squares[61] = new Square(Color.GREEN, whiteQueen, new Position(7,5));
        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));

        Board board = new Board(squares);

        boolean checkMate = main.checkMate(board, Color.BLACK);
        assertEquals(checkMate, true);

//        //Check for false Checkmate
//        squares[45] = new Square(Color.GREEN, null, new Position(5,5));
//        squares[46] = new Square(Color.GREEN, null, new Position(5,6));
//        squares[47] = new Square(Color.GREEN, null, new Position(5,7));
//        squares[61] = new Square(Color.GREEN, null, new Position(7,5));
//        squares[63] = new Square(Color.GREEN, blackKing, new Position(7,7));
//
//        board = new Board(squares);
//
//        checkMate = main.checkMate(board, Color.BLACK);
//        assertEquals(checkMate, false);

    }
}