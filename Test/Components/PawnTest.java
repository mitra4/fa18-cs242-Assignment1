package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class PawnTest {

    Pawn whitePawn = new Pawn(Color.WHITE, new Position(1,1));
    Pawn blackPawn = new Pawn(Color.BLACK, new Position(2,2));

    @Test
    void move() {
        //Check taking black pawn with white pawn
        Square[] squares = new Square[63];
        Board board = new Board(squares);
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        squares[18] = new Square(Color.GREEN, this.blackPawn, new Position(2,2));
        board = new Board(squares);
        Board testBoard = whitePawn.move(board, new Position(2,2));
        assertNotEquals(testBoard, null);

        //Check taking white pawn with black pawn
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        squares[18] = new Square(Color.GREEN, this.blackPawn, new Position(2,2));
        board = new Board(squares);
        testBoard = blackPawn.move(board, new Position(1,1));
        assertNotEquals(testBoard, null);

        //Check white pawn moving one forward to empty space
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        squares[17] = new Square(Color.GREEN, null, new Position(2,1));
        board = new Board(squares);
        testBoard = whitePawn.move(board, new Position(2,1));
        assertNotEquals(testBoard, null);

        //Check moving off board
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        board = new Board(squares);
        testBoard = whitePawn.move(board, new Position(8,8));
        assertEquals(testBoard, null);

        //Check black pawn move one forward
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[10] = new Square(Color.GREEN, null, new Position(1,2));
        squares[18] = new Square(Color.GREEN, this.blackPawn, new Position(2,2));
        board = new Board(squares);
        testBoard = blackPawn.move(board, new Position(1,2));
        assertNotEquals(testBoard, null);

        //Check not moving
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        board = new Board(squares);
        testBoard = whitePawn.move(board, new Position(6,1));
        assertEquals(testBoard, null);

        //Check trying to move into taken space
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,1));
        squares[17] = new Square(Color.GREEN, this.blackPawn, new Position(2,1));
        squares[9] = new Square(Color.GREEN, this.whitePawn, new Position(1,1));
        board = new Board(squares);
        testBoard = whitePawn.move(board, new Position(2,1));
        assertEquals(testBoard, null);

        //Move piece that isn't on board.
        whitePawn.setPosition(new Position(1,1));
        blackPawn.setPosition(new Position(2,2));
        squares[9] = new Square(Color.GREEN, this.blackPawn, new Position(2,2));
        board = new Board(squares);
        testBoard = whitePawn.move(board, new Position(2,2));
        assertEquals(testBoard, null);

    }
}