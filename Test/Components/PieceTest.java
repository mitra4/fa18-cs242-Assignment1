package Components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import Components.*;

import java.awt.*;

class PieceTest {

    Piece pieceWhite = new Piece(Color.WHITE, new Position(1,1));
    Piece pieceBlack = new Piece(Color.BLACK, new Position(2,2));

    @Test
    void setColor() {
        pieceWhite.setColor(Color.WHITE);

    }

    @Test
    void setPosition() {
        pieceWhite.setPosition(new Position(2,0));
    }

    @Test
    void getColor() {
        pieceWhite.getColor();
    }

    @Test
    void getPosition() {
        pieceWhite.getPosition();
    }

    @Test
    void resetBoard() {
        Square[] squares = new Square[63];
        squares[9] = new Square(Color.WHITE, this.pieceWhite, new Position(1,1));
        squares[18] = new Square(Color.WHITE, this.pieceWhite, new Position(2,2));
        Board board = new Board(squares);
        Board newBoard = pieceWhite.resetBoard(board, new Position(2,2));
        assertEquals(newBoard, null);

        squares[9] = new Square(Color.WHITE, this.pieceWhite, new Position(1,1));
        squares[18] = new Square(Color.BLACK, this.pieceBlack, new Position(2,2));
        board = new Board(squares);
        Board secondBoard = pieceWhite.resetBoard(board, new Position(2,2));
        assertNotEquals(secondBoard, null);


    }

    @Test
    void checkForInterimPiece() {
        Square[] squares = new Square[63];
        squares[9] = new Square(Color.WHITE, this.pieceWhite, new Position(1,1));
        squares[18] = new Square(Color.WHITE, this.pieceWhite, new Position(2,2));
        Board board = new Board(squares);
        boolean checker = pieceWhite.checkForInterimPiece(0,0,2,2,squares);
        assertEquals(checker, true);
    }
}