package Components;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PositionTest {

    public Position position = new Position(1,1);

    @Test
    void setRow() {
        this.position.setRow(2);
        assertEquals(position.getRow(), 2);
    }

    @Test
    void setColumn() {
        this.position.setColumn(2);
        assertEquals(position.getColumn(), 2);
    }

    @Test
    void getRow() {
        position.setRow(2);
        assertEquals(position.getRow(), 2);
    }

    @Test
    void getColumn() {
        position.getColumn();
    }
}