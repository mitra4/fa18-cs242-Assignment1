package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class QueenTest {

    Queen whiteQueen = new Queen(Color.WHITE, new Position(3,3));
    Pawn blackPawn = new Pawn(Color.BLACK, new Position(4,4));


    @Test
    void move() {
        // Setup Board
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 27 && i != 36)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }
        squares[27] = new Square(Color.GREEN, whiteQueen, new Position(3,3));
        squares[36] = new Square(Color.GREEN, blackPawn, new Position(4,4));
        Board board = new Board(squares);

        //Check out of bounds
        Board testBoard = whiteQueen.move(board, new Position(9,9));
        assertEquals(testBoard, null);

        //Check invalid move
        board = new Board(squares);
        testBoard = whiteQueen.move(board, new Position(7,1));
        assertEquals(testBoard, null);

        //Check forward movement with no capture
        board = new Board(squares);
        testBoard = whiteQueen.move(board, new Position(4,3));
        assertNotEquals(testBoard, null);

        //Check backwards movement
        testBoard = whiteQueen.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check move right
        testBoard = whiteQueen.move(testBoard, new Position(3,4));
        assertNotEquals(testBoard, null);

        //Check move left
        testBoard = whiteQueen.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check up and left movement
        testBoard = whiteQueen.move(board, new Position(4,2));
        assertNotEquals(testBoard, null);

        //Check down and right movement
        testBoard = whiteQueen.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Check up and right movement
        testBoard = whiteQueen.move(testBoard, new Position(4,4));
        assertNotEquals(testBoard, null);

        //Check down and left movement
        testBoard = whiteQueen.move(testBoard, new Position(3,3));
        assertNotEquals(testBoard, null);

        //Illegal jump over piece diagonal
        squares[36] = new Square(Color.GREEN, blackPawn, new Position(4,4));
        board = new Board(squares);
        testBoard = whiteQueen.move(board, new Position(5,5));
        assertEquals(testBoard, null);

    }
}