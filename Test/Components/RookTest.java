package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class RookTest {

    Rook whiteRook = new Rook(Color.WHITE, new Position(1,1));
    Rook blackRook = new Rook(Color.BLACK, new Position(6,1));

    @Test
    void move() {
        //Setup board
        Square[] squares = new Square[64];
        Position position = new Position(0,0);
        for (int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }
            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i != 9 && i != 49)
            {
                squares[i] = new Square(Color.GREEN, null, position);
            }
        }
        Board board = new Board(squares);
        squares[9] = new Square(Color.GREEN, whiteRook, new Position(1,1));
        squares[49] = new Square(Color.GREEN, blackRook, new Position(6,1));
        board = new Board(squares);

        //Check out of bounds
        Board testBoard = whiteRook.move(board, new Position(9,9));
        assertEquals(testBoard, null);

        //Check invalid move
        board = new Board(squares);
        testBoard = whiteRook.move(board, new Position(2,2));
        assertEquals(testBoard, null);

        //Check forward movement with no capture
        board = new Board(squares);
        testBoard = whiteRook.move(board, new Position(3,1));
        assertNotEquals(testBoard, null);

        //Check backwards movement
        testBoard = whiteRook.move(testBoard, new Position(1,1));
        assertNotEquals(testBoard, null);

        //Check move right
        testBoard = whiteRook.move(testBoard, new Position(1,3));
        assertNotEquals(testBoard, null);

        //Check move left
        testBoard = whiteRook.move(testBoard, new Position(1,1));
        assertNotEquals(testBoard, null);

        //Check invalid upwards movement (Piece in way)
        testBoard = whiteRook.move(testBoard, new Position(7,1));
        assertEquals(testBoard, null);

        //Check invalid downwards movement (Piece in way)
        board = new Board(squares);
        testBoard = blackRook.move(board, new Position(0,1));
    }
}