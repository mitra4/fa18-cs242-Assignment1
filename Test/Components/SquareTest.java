package Components;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {
    Piece piece = new Piece(Color.WHITE, new Position(1,1));
    Square square = new Square(Color.WHITE, piece, new Position(1,1));

    @Test
    void setColor() {
        square.setColor(Color.BLACK);
    }

    @Test
    void setPiece() {
        square.setPiece(piece);
    }

    @Test
    void setPosition() {
        square.setPosition(new Position(1,1));
    }

    @Test
    void getPosition() {
        square.getPosition();
    }

    @Test
    void getColor() {
        square.getColor();
    }

    @Test
    void getPiece() {
        square.getPiece();
    }
}