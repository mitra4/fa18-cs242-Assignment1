package Components;

import java.awt.*;

public class Bishop extends Piece {

    public Bishop(Color color, Position position)
    {
        super(color, position);
    }

    /**
     *
     * @param board board with all the pieces on it
     * @param new_position the spot you want to move to
     * @return either a new board which would be a valid move, or null if move was invalid.
     */
    public Board move(Board board, Position new_position)
    {
        //Check for out of bounds
        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move 1");
            return null;
        }

        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        //Check that you actually move
        if (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow())
        {
            System.out.println("No move made");
            return null;
        }

        //Check for invalid move
        if (Math.abs(old_position.getColumn() - new_position.getColumn()) != Math.abs((old_position.getRow() - new_position.getRow())))
        {
            System.out.println("Invalid move 2");
            return null;
        }

        //Moving up and right
        if(new_position.getColumn() > old_position.getColumn() && new_position.getRow() > old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() + 1; row <= new_position.getRow(); row++)
            {
                for(int col = old_position.getColumn() + 1; col <= new_position.getColumn(); col++)
                {
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 3");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going up and to the left
        else if(new_position.getColumn() < old_position.getColumn() && new_position.getRow() > old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() + 1; row <= new_position.getRow(); row++)
            {
                for(int col = old_position.getColumn() - 1; col >= new_position.getColumn(); col--)
                {
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 4");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going down and to the right
        else if(new_position.getColumn() > old_position.getColumn() && new_position.getRow() < old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() - 1; row >= new_position.getRow(); row--)
            {
                for(int col = old_position.getColumn() + 1; col <= new_position.getColumn(); col++)
                {
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 5");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going down and left.
        else if(new_position.getColumn() < old_position.getColumn() && new_position.getRow() < old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() - 1; row >= new_position.getRow(); row--)
            {
                for(int col = old_position.getColumn() - 1; col >= new_position.getColumn(); col--)
                {
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 6");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }
        System.out.println("Must move piece");
        return null;
    }
}
