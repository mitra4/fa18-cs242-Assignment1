package Components;

import java.awt.Color;
import java.util.ArrayList;

public class Board {
    private Square[] squares;

    //Constructor
    public Board(Square[] squares)
    {
        this.squares = squares;
    }

    //Setter
    public void setSquares(Square[] squares)
    {
        this.squares = squares;
    }

    //Getter
    public Square[] getSquares()
    {
        return this.squares;
    }

    /**
     *
     * @param color Color of all pieces to find
     * @return Return an array list of all pieces of certain color
     */
    public ArrayList<Piece> getPiecesOfColor(Color color)
    {
        ArrayList<Piece> colorPieces = new ArrayList<Piece>();
        Square[] squares = this.getSquares();
        //Iterate over all squares and find the ones of correct color.
        for(int i = 0; i < squares.length; i++)
        {
            if (squares[i].getPiece() == null)
            {
                continue;
            }
            else if (squares[i].getPiece().getColor() == color)
            {
                colorPieces.add(squares[i].getPiece());
            }
        }
        return colorPieces;
    }


}
