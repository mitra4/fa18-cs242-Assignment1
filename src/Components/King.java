package Components;

import java.awt.*;

public class King extends Piece {

    public King(Color color, Position position) {
        super(color, position);
    }

    //TODO Modularize diagonal check
    //public boolean checkDiagonal(Components.Position king_position, Components.Square[] squares, Color king_color, int row_direction, int col_direction)


    public boolean isKingInCheck(Board board) {
        Position king_position = this.getPosition();
        Color king_color = this.getColor();
        Square[] squares = board.getSquares();

        //Following 8 statements are all to check for knights in the surrounding areas.
        if (king_position.getRow() + 2 <= 7 && king_position.getColumn() + 1 <= 7) {
            Position checkPosition = new Position(king_position.getRow() + 2, king_position.getColumn() + 1);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() + 2 <= 7 && king_position.getColumn() - 1 >= 0) {
            Position checkPosition = new Position(king_position.getRow() + 2, king_position.getColumn() - 1);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() - 2 >= 0 && king_position.getColumn() - 1 >= 0) {
            Position checkPosition = new Position(king_position.getRow() - 2, king_position.getColumn() - 1);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() - 2 >= 0 && king_position.getColumn() + 1 <= 7) {
            Position checkPosition = new Position(king_position.getRow() - 2, king_position.getColumn() + 1);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }
        else if (king_position.getRow() + 1 <= 7 && king_position.getColumn() + 2 <= 7) {
            Position checkPosition = new Position(king_position.getRow() + 1, king_position.getColumn() + 2);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() + 1 <= 7 && king_position.getColumn() - 2 >= 0) {
            Position checkPosition = new Position(king_position.getRow() + 1, king_position.getColumn() - 2);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() - 1 >= 0 && king_position.getColumn() - 2 >= 0) {
            Position checkPosition = new Position(king_position.getRow() - 1, king_position.getColumn() - 2);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        else if (king_position.getRow() - 1 >= 0 && king_position.getColumn() + 2 <= 7) {
            Position checkPosition = new Position(king_position.getRow() -1, king_position.getColumn() + 2);
            Square new_square = squares[checkPosition.getRow() * 8 + checkPosition.getColumn()];
            if(new_square.getPiece() instanceof Knight && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
        }

        //Check for bishops and pawns
        Position interimPosition = new Position(king_position.getRow(), king_position.getColumn());
        int iterator = 1;
        //Checks up and Right
        while(interimPosition.getRow() < 7 && interimPosition.getColumn() < 7)
        {
            interimPosition.setRow(interimPosition.getRow() + 1);
            interimPosition.setColumn(interimPosition.getColumn() + 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            //Check for pawns for white king
            if (king_color == Color.WHITE && iterator == 1)
            {
                if (new_square.getPiece() instanceof Pawn && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Bishop || new_square.getPiece() instanceof Queen || new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }

            iterator++;

        }//End while

        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1;
        //Checks up and left
        while(interimPosition.getRow() < 7 && interimPosition.getColumn() >= 1)
        {
            interimPosition.setRow(interimPosition.getRow() + 1);
            interimPosition.setColumn(interimPosition.getColumn() - 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            //Check for pawns for white king
            if (king_color == Color.WHITE && iterator == 1)
            {
                if ((new_square.getPiece() instanceof Pawn || new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            //Check for any other piece up and left.
            if((new_square.getPiece() instanceof Bishop || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }

            iterator++;

        }//End while

        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1;
        //Checks down and right
        while(interimPosition.getRow() >=1 && interimPosition.getColumn() < 7)
        {
            interimPosition.setRow(interimPosition.getRow() - 1);
            interimPosition.setColumn(interimPosition.getColumn() + 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            //Check for pawns for white king
            if (king_color == Color.BLACK && iterator == 1)
            {
                if ((new_square.getPiece() instanceof Pawn || new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Bishop || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }

            iterator++;

        }//End while

        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1;
        //Checks down and left
        while(interimPosition.getRow() >=1 && interimPosition.getColumn() >= 1)
        {
            interimPosition.setRow(interimPosition.getRow() - 1);
            interimPosition.setColumn(interimPosition.getColumn() - 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            //Check for pawns for white king
            if (king_color == Color.BLACK && iterator == 1)
            {
                if ((new_square.getPiece() instanceof Pawn || new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Bishop || new_square.getPiece() instanceof Queen ) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }

            iterator++;

        }//End while

        //Check for rooks/kings/queens
        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());

        //Check forward
        iterator = 1;
        while(interimPosition.getRow() < 7)
        {
            interimPosition.setRow(interimPosition.getRow() + 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            if (iterator == 1)
            {
                if ((new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Rook || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
            iterator++;
        }

        //Check behind
        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1; //how many moves removed are we from the king
        while(interimPosition.getRow() >=  1)
        {
            interimPosition.setRow(interimPosition.getRow() - 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            if (iterator == 1)
            {
                if ((new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Rook || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
            iterator++;
        }

        //Check right
        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1;
        while(interimPosition.getColumn() < 7)
        {
            interimPosition.setColumn(interimPosition.getColumn() + 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            if (iterator == 1)
            {
                if ((new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Rook || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
            iterator++;
        }

        //Check left
        interimPosition.setRow(king_position.getRow());
        interimPosition.setColumn(king_position.getColumn());
        iterator = 1;
        while(interimPosition.getColumn() >= 1)
        {
            interimPosition.setColumn(interimPosition.getColumn() - 1);
            Square new_square = squares[interimPosition.getRow() * 8 + interimPosition.getColumn()];
            if (iterator == 1)
            {
                if ((new_square.getPiece() instanceof  King) && king_color != new_square.getPiece().getColor())
                {
                    return true;
                }
            }
            if((new_square.getPiece() instanceof Rook || new_square.getPiece() instanceof Queen) && king_color != new_square.getPiece().getColor())
            {
                return true;
            }
            iterator++;
        }

        return false;
    }

    /**
     *
     * @param board board with all the pieces on it
     * @param new_position the spot you want to move to
     * @return either a new board which would be a valid move, or null if move was invalid.
     */
    public Board move(Board board, Position new_position)
    {
        //Check for out of bounds
        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move");
            return null;
        }

        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        //Check that you actually move
        if (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow())
        {
            System.out.println("No move made");
            return null;
        }
        //Can only move in one space in any direction
        if (Math.abs(new_position.getColumn() - old_position.getColumn()) < 2 && Math.abs(new_position.getRow() - old_position.getRow()) < 2)
        {
            Board new_board = this.resetBoard(board, new_position);
            if(new_board == null)
            {
                System.out.println("Illegal move");
                return null;
            }
            //Check to see if move puts king in check, return null if king is in check to redo move.
            boolean inCheck = isKingInCheck(new_board);
            if(inCheck == true)
            {
                System.out.println("Components.King is in check");
                return null;
            }
            return new_board;
        }
        return null;
    }
}