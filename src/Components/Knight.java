package Components;

import java.awt.*;

public class Knight extends Piece {

    public Knight(Color color, Position position)
    {
        super(color, position);
    }

    /**
     *
     * @param board board with all the pieces on it
     * @param new_position the spot you want to move to
     * @return either a new board which would be a valid move, or null if move was invalid.
     */
    public Board move(Board board, Position new_position)
    {
        //Check for out of bounds
        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move");
            return null;
        }

        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        //Check that you actually move
        if (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow())
        {
            System.out.println("No move made");
            return null;
        }

        //Knights can only move in L's but they can jump over so we don't worry about intermediate pieces.
        //L's are either 2 across one up or down ot one across and 2 up or down.
        if (((Math.abs(old_position.getRow() - new_position.getRow()) == 2) && Math.abs(old_position.getColumn() - new_position.getColumn()) == 1) || ((Math.abs(old_position.getRow() - new_position.getRow()) == 1) && Math.abs(old_position.getColumn() - new_position.getColumn()) == 2))
        {
            return this.resetBoard(board, new_position);
        }

        return null;
    }
}