package Components;

import javax.swing.plaf.synth.ColorType;
import java.awt.Color;
import java.util.ArrayList;

public class Main {
    private Board board;

    /**
     *
     * @return a Board ready to play with all pieces.
     */
    public Board boardSetup()
    {
        Square[] pieces = new Square[64];
        Position position = new Position(0,0);
        //Iterate thru entire array and set positions for each piece.
        for(int i = 0; i < 64; i++)
        {
            if(i != 0 && i % 8 == 0)
            {
                position.setRow(position.getRow() + 1);
                position.setColumn(0);
            }

            else if(i != 0 && i % 8 != 0)
            {
                position.setColumn(position.getColumn() + 1);
            }

            if (i == 0 || i == 7)
            {
                Rook whiteRook = new Rook(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whiteRook, position);

            }
            else if (i == 1 || i == 6)
            {
                Knight whiteKnight = new Knight(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whiteKnight, position);
            }

            else if (i == 2 || i == 5)
            {
                Bishop whiteBishop = new Bishop(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whiteBishop, position);
            }
            else if (i == 3)
            {
                King whiteKing = new King(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whiteKing, position);
            }
            else if(i == 4)
            {
                Queen whiteQueen = new Queen(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whiteQueen, position);
            }
            else if(i >= 8 && i <= 15)
            {
                Pawn whitePawn = new Pawn(Color.WHITE, position);
                pieces[i] = new Square(Color.GREEN, whitePawn, position);
            }

            else if(i >= 48 && i <= 55)
            {
                Pawn blackPawn = new Pawn(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackPawn, position);
            }
            else if (i == 56 || i == 63)
            {
                Rook blackRook = new Rook(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackRook, position);
            }
            else if (i == 57 || i == 62)
            {
                Knight blackKnight = new Knight(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackKnight, position);
            }
            else if (i == 58 || i == 61)
            {
                Bishop blackBishop = new Bishop(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackBishop, position);
            }
            else if (i == 59)
            {
                King blackKing = new King(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackKing, position);
            }
            else if (i == 60)
            {
                Queen blackQueen = new Queen(Color.BLACK, position);
                pieces[i] = new Square(Color.GREEN, blackQueen, position);
            }
            else
            {
                pieces[i] = new Square(Color.GREEN, null, position);
            }

        }
        Board retBoard = new Board(pieces);
        return retBoard;
    }

    /**
     *
     * @param board board that is currently being played
     * @param color Color side you want to check
     * @return Whether or not the move would result in check for the king.
     */
    public boolean checkMove(Board board, Color color)
    {
        ArrayList<Piece> piecesLeft = board.getPiecesOfColor(color);
        King colorKing = null;
        //Find the king of correct color.
        for (Piece piece : piecesLeft)
        {
            if (piece instanceof  King)
            {
                colorKing = new King(piece.getColor(), piece.getPosition());
            }
        }

        boolean colorKingCheck = colorKing.isKingInCheck(board);
        if (colorKingCheck == true)
        {
            return true;
        }

        return false;
    }

    /**
     *
     * @param board board with pieces on it
     * @param color Color we're seeing if it's checkmate for
     * @return boolean value of whether or not checkmate is possible
     */
    public boolean checkMate(Board board, Color color)
    {
        ArrayList<Piece> piecesLeft = board.getPiecesOfColor(color);
        King colorKing = null;
        Board originalBoard = new Board(board.getSquares());
        Board testerBoard = new Board(board.getSquares());
        //Find king of Color color
        for (Piece piece : piecesLeft)
        {
            if (piece instanceof  King)
            {
                colorKing = new King(piece.getColor(), piece.getPosition());
            }
        }

        //Try all moves for all pieces. If move is illegal then continue loop.
        for (Piece piece: piecesLeft)
        {
            for (int row = 0; row < 8; row++)
            {
                for (int column = 0; column < 8; column ++)
                {
                    testerBoard.setSquares(originalBoard.getSquares());
                    Position checkPosition = new Position(row, column);
                    //Try all moves for pawn
                    if (piece instanceof Pawn)
                    {
                        Pawn newPawn = new Pawn(piece.getColor(), piece.getPosition());
                        Board checkBoard = newPawn.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }

                    //Try all moves for Rook
                    else if (piece instanceof Rook)
                    {
                        Rook newRook = new Rook(piece.getColor(), piece.getPosition());
                        Board checkBoard = newRook.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }
                    //Try all moves for knight
                    else if (piece instanceof Knight)
                    {
                        Knight newKnight = new Knight(piece.getColor(), piece.getPosition());
                        Board checkBoard = newKnight.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }
                    //Try all moves for bishop
                    else if (piece instanceof Bishop)
                    {
                        Bishop newBishop = new Bishop(piece.getColor(), piece.getPosition());
                        Board checkBoard = newBishop.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }
                    //TRy all moves for Queen
                    else if (piece instanceof Queen)
                    {
                        Queen newQueen = new Queen(piece.getColor(), piece.getPosition());
                        Board checkBoard = newQueen.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }
                    //Try all moves for king.
                    else if (piece instanceof King)
                    {
                        King newKing = new King(piece.getColor(), piece.getPosition());
                        Board checkBoard = newKing.move(testerBoard, checkPosition);
                        if (checkBoard == null)
                        {
                            continue;
                        }
                        boolean kingCheck = colorKing.isKingInCheck(checkBoard);
                        if (kingCheck == false)
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public Main()
    {
        this.board = boardSetup();
        boolean gameOver = false;
        int iterator = 0;
        //Commented out to use for future with gui and game.
//        while(gameOver == false)
//        {
//            if(iterator % 2 == 0) //Signifies white turn
//            {
//                boolean staleMate = checkMate(board, Color.WHITE); //If you can't make a legal move it's a stalemate
//                boolean checkMate = checkMate(board, Color.BLACK); //If opponent can't make a legal move now then Checkmate
//            }
//            else
//            {
//                boolean staleMate = checkMate(board, Color.BLACK);
//                boolean checkMate = checkMate(board, Color.WHITE);
//            }
//            iterator++;
//            if (iterator == 10)
//            {
//                gameOver = true;
//            }
//        }

    }


}
