package Components;

import java.awt.*;


public class Pawn extends Piece {

    public Pawn(Color color, Position position)
    {
        super(color, position);
    }

    /**
     *
     * @param board Components.Board that holds all the chess pieces
     * @param new_position Components.Position that you're trying to move Pawn to
     * @return New board with moved pawn or null if move was invalid. Check for null on other end.
     */
    public Board move(Board board, Position new_position)
    {
        //Check illegal moves
        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move");
            return null;
        }

        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        //Check that you actually move
        if (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow())
        {
            System.out.println("No move made");
            return null;
        }

        if (this.getColor() == Color.BLACK) //Color only really matters for pawns since they can only go forward
        {
            //Try going diagonal
            if ((new_position.getRow() == old_position.getRow() - 1) && ((new_position.getColumn() == old_position.getColumn() - 1) || (new_position.getColumn() == old_position.getColumn() + 1)))
            {
                if (new_square.getPiece() != null)
                {
                    return this.resetBoard(board, new_position);
                }
                else
                {
                    System.out.println("Invalid move 1");
                    return null;
                }
            }

            //Try going forward
            if ((new_position.getRow() == old_position.getRow() - 1) && (new_position.getColumn() == old_position.getColumn()))
            {
                if (new_square.getPiece() != null)
                {
                    System.out.println("Invalid move 2");
                    return null;
                }
                else
                {
                    return this.resetBoard(board, new_position);
                }
            }
        }

        else
        {
            //Try going diagonal
            if ((new_position.getRow() == old_position.getRow() + 1) && ((new_position.getColumn() == old_position.getColumn() - 1) || (new_position.getColumn() == old_position.getColumn() + 1)))
            {
                if (new_square.getPiece() != null)
                {
                    return this.resetBoard(board, new_position);
                }
                else
                {
                    System.out.println("Invalid move 3");
                    return null;
                }
            }

            //Try going straight
            if ((new_position.getRow() == old_position.getRow() + 1) && (new_position.getColumn() == old_position.getColumn()))
            {
                if (new_square.getPiece() != null)
                {
                    System.out.println("Invalid move 4");
                    return null;
                }
                else
                {
                    return this.resetBoard(board, new_position);
                }
            }
        }

        return null;
    }
}
