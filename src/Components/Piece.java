package Components;

import java.awt.Color;

public class Piece {
    private Color color; //Black starts on far side of board, white on close side.
    private Position position;

    //Constructor
    public Piece(Color color, Position position)
    {
        this.position = position;
        this.color = color;
    }

    //Set the color of piece
    public void  setColor(Color color)
    {
        this.color = color;
    }

    //Set position of piece
    public void setPosition(Position position)
    {
        this.position = position;
    }

    //Get Color of piece
    public Color getColor()
    {
        return this.color;
    }

    //Get position of piece.
    public Position getPosition()
    {
        return this.position;
    }

    /**
     *
     * @param board The chess board which holds all pieces and positions
     * @param new_position spot you are trying to move the piece to
     * @return new board with moved piece or null if move was invalid. Do null check on other end to see if move must be redone.
     * Code exists to eliminate duplicate code in move and to check you're not taking over your own piece.
     */
    public Board resetBoard(Board board, Position new_position)
    {
        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        if (new_square.getPiece() != null) {
            Piece newPositionPiece = new_square.getPiece();
            if(newPositionPiece.getColor() == this.getColor())
            {
                System.out.println("Invalid move 5");
                return null;
            }
            else
            {
                this.setPosition(new_position);
                new_square.setPiece(this);
                old_square.setPiece(null);
                squares[new_position.getRow() * 8 + new_position.getColumn()] = new_square;
                squares[old_position.getRow() * 8 + old_position.getColumn()] = old_square;
                board.setSquares(squares);
                this.setPosition(new_position);
                return board;
            }
        }

        else {
            if(new_square.getPiece() instanceof  King)
            {
                return null;
            }
            this.setPosition(new_position);
            new_square.setPiece(this);
            old_square.setPiece(null);
            squares[new_position.getRow() * 8 + new_position.getColumn()] = new_square;
            squares[old_position.getRow() * 8 + old_position.getColumn()] = old_square;
            this.setPosition(new_position);
            board.setSquares(squares);
            return board;
        }
    }

    /**
     *
     * @param row_iterator just keeps track of how many rows you've moved. Since we're going diagonal, row must equal column movement
     * @param col_iterator Keep track of number of columns iterated
     * @param row current row on board that you're checking
     * @param col current col on board that you're checking
     * @param squares Array of all the squares on the board, to check for pieces
     * @return new board with made changes, or possibly null if invalid move.
     */
    public boolean checkForInterimPiece(int row_iterator, int col_iterator, int row, int col, Square[] squares)
    {
        if (row_iterator == col_iterator)
        {
            Square interimSquare = squares[row * 8 + col];
            if (interimSquare.getPiece() != null)
            {
                System.out.println("Invalid move");
                return true;
            }
        }
        return false;
    }
}


