package Components;

public class Position {
    private int row; //row on board
    private int column; //column on board

    //Constructor
    public Position(int row, int column)
    {
        this.row = row;
        this.column = column;
    }

    //Setter for row
    public void setRow(int row)
    {
        this.row = row;
    }

    //Setter for column
    public void setColumn(int column)
    {
        this.column = column;
    }

    //Getter for row
    public int getRow()
    {
        return this.row;
    }

    //Getter for column
    public int getColumn()
    {
        return this.column;
    }
}
