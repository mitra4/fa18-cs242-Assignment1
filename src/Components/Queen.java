package Components;

import java.awt.*;

public class Queen extends Piece {

    public Queen(Color color, Position position)
    {
        super(color, position);
    }

    /**
     *
     * @param board board with all the pieces on it
     * @param new_position the spot you want to move to
     * @return either a new board which would be a valid move, or null if move was invalid.
     */
    public Board move(Board board, Position new_position)
    {
        //Check for out of bounds
        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move 1");
            return null;
        }


        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];

        //Check if move was even made
        if (new_position.getRow() == old_position.getRow() && new_position.getColumn() == old_position.getColumn())
        {
            System.out.println("No move made");
            return null;
        }

        //Not a diagonal or horizontal or vertical move.
        if ((Math.abs(old_position.getColumn() - new_position.getColumn()) != Math.abs((old_position.getRow() - new_position.getRow()))) && (new_position.getColumn() != old_position.getColumn() && new_position.getRow() != old_position.getRow()))
        {
            System.out.println("Invalid move 2");
            return null;
        }

        //Move up and right
        if(new_position.getColumn() > old_position.getColumn() && new_position.getRow() > old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() + 1; row <= new_position.getRow(); row++)
            {
                for(int col = old_position.getColumn() + 1; col <= new_position.getColumn(); col++)
                {
                    //Check to make sure there's no piece in the way
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 3");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going up and to the left
        else if(new_position.getColumn() < old_position.getColumn() && new_position.getRow() > old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() + 1; row <= new_position.getRow(); row++)
            {
                for(int col = old_position.getColumn() - 1; col >= new_position.getColumn(); col--)
                {
                    //Check to make sure there's no piece in the way
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 4");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going down and to the right
        else if(new_position.getColumn() > old_position.getColumn() && new_position.getRow() < old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() - 1; row >= new_position.getRow(); row--)
            {
                for(int col = old_position.getColumn() + 1; col <= new_position.getColumn(); col++)
                {
                    //Check to make sure that there's no piece in the way
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 5");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }

        //Going down and left.
        else if(new_position.getColumn() < old_position.getColumn() && new_position.getRow() < old_position.getRow())
        {
            int row_iterator = 0;
            for (int row = old_position.getRow() - 1; row >= new_position.getRow(); row--)
            {
                for(int col = old_position.getColumn() - 1; col >= new_position.getColumn(); col--)
                {
                    //Check to make sure there's no piece in the way.
                    int col_iterator = 0;
                    boolean interimPiece = this.checkForInterimPiece(row_iterator, col_iterator, row, col, squares);
                    if (interimPiece == true && (row != new_position.getRow() || col != new_position.getColumn()))
                    {
                        System.out.println("Invalid move 6");
                        return null;
                    }
                    col_iterator++;
                }
                row_iterator++;
            }
            return this.resetBoard(board, new_position);
        }
        //We're just going up the rows of the board, so we have to check each row in column for piece in the way.
        else if (new_position.getColumn() == old_position.getColumn())
        {
            if (new_position.getRow() < old_position.getRow())
            {
                //Iterate thru all intermediate squares to check if there's a piece in the way.
                for(int i = old_position.getRow() -1; i >= new_position.getRow(); i--)
                {
                    Square interimSquare = squares[i * 8 + new_position.getColumn()];
                    if (interimSquare.getPiece() != null)
                    {
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else if (new_position.getRow() > old_position.getRow())
            {
                //ITerate to make sure you're not jumping any pieces.
                for(int i = old_position.getRow() + 1; i <= new_position.getRow(); i++)
                {
                    Square interimSquare = squares[i * 8 + new_position.getColumn()];
                    if (interimSquare.getPiece() != null)
                    {
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else
            {
                return null;
            }
        }

        //Going across the board. Staying in the same row.
        else
        {
            if (new_position.getColumn() < old_position.getColumn())
            {
                for(int i = old_position.getColumn() - 1; i >= new_position.getColumn(); i--)
                {
                    //Check to make sure that you're not jumping any pieces
                    Square interimSquare = squares[old_position.getRow() * 8 + i];
                    if (interimSquare.getPiece() != null)
                    {
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else if (new_position.getColumn() > old_position.getColumn())
            {
                for(int i = old_position.getColumn() + 1; i <= new_position.getColumn(); i++)
                {
                    //Check to make sure you're not jumping any squares.
                    Square interimSquare = squares[old_position.getRow() * 8 + i];
                    if (interimSquare.getPiece() != null)
                    {
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else
            {
                return null;
            }
        }

    }
}
