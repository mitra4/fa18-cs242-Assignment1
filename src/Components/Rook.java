package Components;

import java.awt.*;

public class Rook extends Piece {

    public Rook(Color color, Position position)
    {
        super(color, position);
    }

    /**
     *
     * @param board board with all the pieces on it
     * @param new_position the spot you want to move to
     * @return either a new board which would be a valid move, or null if move was invalid.
     */
    public Board move(Board board, Position new_position)
    {

        if (new_position.getColumn() > 7 || new_position.getRow() > 7 || new_position.getRow() < 0 || new_position.getColumn() < 0)
        {
            System.out.println("Invalid move 1");
            return null;
        }

        Position old_position = this.getPosition();
        Square[] squares = board.getSquares();
        Square new_square = squares[new_position.getRow() * 8 + new_position.getColumn()];//Square you want to move to
        Square old_square = squares[old_position.getRow() * 8 + old_position.getColumn()];//Current Square

        //Check that you actually move
        if (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow())
        {
            System.out.println("No move made");
            return null;
        }

        //Not moving in straight line
        if ((new_position.getColumn() != old_position.getColumn() && new_position.getRow() != old_position.getRow()) || (new_position.getColumn() == old_position.getColumn() && new_position.getRow() == old_position.getRow()))
        {
            System.out.println("Invalid move 2");
            return null;
        }

        //We're just going up the rows of the board, so we have to check each row in column for piece in the way.
        else if (new_position.getColumn() == old_position.getColumn())
        {
            if (new_position.getRow() < old_position.getRow())
            {
                //Iterate thru all intermediate squares to check if there's a piece in the way.
                for(int i = old_position.getRow() - 1; i >= new_position.getRow(); i--)
                {
                    Square interimSquare = squares[i * 8 + new_position.getColumn()];
                    if (interimSquare.getPiece() != null)
                    {
                        System.out.println("Invalid move 3");
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else if (new_position.getRow() > old_position.getRow())
            {
                //Check for any pieces in the way of your move
                for(int i = old_position.getRow() + 1; i <= new_position.getRow(); i++)
                {
                    Square interimSquare = squares[i * 8 + new_position.getColumn()];
                    if (interimSquare.getPiece() != null)
                    {
                        System.out.println("Invalid move 4");
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else
            {
                System.out.println("Invalid move 5");
                return null;
            }
        }

        //We're going across the columns of the board
        else
        {
            //Move left
            if (new_position.getColumn() < old_position.getColumn())
            {
                for(int i = old_position.getColumn() - 1; i >= new_position.getColumn(); i--)
                {
                    //Check for pieces in the way
                    Square interimSquare = squares[old_position.getRow() * 8 + i];
                    if (interimSquare.getPiece() != null)
                    {
                        System.out.println("Invalid move 6");
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            //Move right
            else if (new_position.getColumn() > old_position.getColumn())
            {
                for(int i = old_position.getColumn() + 1; i <= new_position.getColumn(); i++)
                {
                    //Check for pieces in the way
                    Square interimSquare = squares[old_position.getRow() * 8 + i];
                    if (interimSquare.getPiece() != null)
                    {
                        System.out.println("Invalid move 7");
                        return null;
                    }
                }
                return this.resetBoard(board, new_position);
            }
            else
            {
                System.out.println("Invalid move 8");
                return null;
            }
        }
    }
}