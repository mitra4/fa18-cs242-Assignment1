package Components;

import java.awt.Color;

//Components.Square Class holds information about each Components.Position on the board
public class Square {
    private  Color color; //Color of square (Eventually gonna be black and white tiles)
    private Piece piece; //Piece if any in square. No piece means null
    private Position position; //Position of square on board. Assigned once and doesn't change

    //Constructor
    public Square(Color color, Piece piece, Position position)
    {
        this.color = color;
        this.piece = piece;
        this.position = position;
    }

    //Setter for color
    public void setColor(Color color)
    {
        this.color = color;
    }

    //Setter for piece on square
    public void setPiece(Piece piece)
    {
        this.piece = piece;
    }

    //Setter for position of square
    public void setPosition(Position position)
    {
        this.position = position;
    }

    //Getter for position
    public Position getPosition()
    {
        return this.position;
    }

    //Getter for color
    public Color getColor()
    {
        return this.color;
    }

    //Getter for piece
    public Piece getPiece()
    {
        return this.piece;
    }
}
